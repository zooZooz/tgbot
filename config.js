require('dotenv');
const config = {};

config.botToken = process.env.BOT_TOKEN || '';
config.botDomain = process.env.BOT_DOMAIN || '';
config.inviteLink = '';

config.logDateFormat = 'DD-MM-YYYY hh:mm:ss';
config.logFile = 'bot.log';
config.logLevel = 'info';

config.invoiceLink = '';
config.invoiceRK = '';

config.dbHost = process.env.DB_HOST || '';
config.dbUser = process.env.DB_USER || '';
config.dbPass = process.env.DB_PASS || '';
config.dbName = process.env.DB_NAME || '';

config.httpToken = 'Gh75fe1r23';
config.httpPort = 8080;

module.exports = config;

const locale = {};

/* текст, отправляющийся пользователю по команде старт */
locale.start = "";

/* текст на кнопке */
locale.button = "";

/* сообщение, если ещё не оплачено */
locale.notPaid = "";

/* сообщение, с которым будет отправлена кнопка */
locale.linkMsg = "";

/* предупреждение о том, что пользователь должен разрешить писать боту */
locale.warning = "";
locale.warningPay = "";


/* сообщение после успешной оплаты */
locale.paySuccess = "" + locale.warning;

locale.alreadyPaid = "";

/* стандартное сообщение */
locale.splash = "";

/* сообщение с ссылкой на оплату  */
locale.invoiceMessage = "" + locale.warning;

module.exports = locale;
